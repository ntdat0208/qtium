package testng;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.xml.XmlTest;
import org.xml.sax.SAXException;

import qtium.driver.QTiumDriverFactory;
import qtium.driver.QTiumDriverManager;
import qtium.driver.QTiumWebDriver;
import qtium.helper.xml.DeviceConfigItem;
import qtium.helper.xml.XMLParser;
import selenium.QTiumAutomation;
import selenium.QTiumSetting;

//@author: phuc.thai
//date: 25-Jun-2014
//issue: QG-17 - Run multiple test case on different browsers

public class QTiumTestListener extends QTiumSetting implements ITestListener {

	/*
	 * private static ThreadLocal<QTiumDriverManager> _qtiumDriverManager = new
	 * ThreadLocal<QTiumDriverManager>(){
	 * 
	 * @Override protected QTiumDriverManager initialValue() { return new
	 * QTiumDriverManager(); } };
	 */

	@Override
	public void onFinish(ITestContext arg0) {
//		QTiumAutomation.sendEmail();
	}

	@Override
	public void onStart(ITestContext testContext) {
		String log = String.format("Test[%s] - thread[%d] ", testContext.getName(), Thread.currentThread().getId());
//		System.out.println(log);
		logger.debug(log);
		
		outputdirect = testContext.getOutputDirectory();

		XmlTest xml = testContext.getCurrentXmlTest();
		String testName = xml.getName();
		
		String xmlPath = testContext.getSuite().getXmlSuite().getFileName();
		
		DeviceConfigItem deviceConfigItem = null;
		try {
			deviceConfigItem = XMLParser.parseXMLToList(xmlPath, testName);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		QTiumWebDriver qtiumDriver;

		try {
			qtiumDriver = QTiumDriverFactory.createInstance(deviceConfigItem);

			QTiumDriverManager.setQTiumDriver(qtiumDriver);

			String strLog = String.format("Created QTiumDriver object[%d] - on thread[%d] ", qtiumDriver.hashCode(),
					Thread.currentThread().getId());
			System.out.println(strLog);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailure(ITestResult result) {
		try {
			QTiumAutomation.takeSnapShot(result, outputdirect);
			Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"curl -X POST -H 'Content-type:application/json' --data \"{'text':'ERROR'}\" https://hooks.slack.com/services/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 && exit\""); 

			System.err.println("onTestFailure: failed!!!");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestStart(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		System.err.println("passed!!!");
		try {
			//QTiumAutomation.sleep(900);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static final Log logger = LogFactory.getLog(QTiumTestListener.class);
	
	private static String outputdirect;
}
