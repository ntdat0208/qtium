package qtium.driver.selenium.windows;

import java.io.File;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import qtium.QTiumConstants;
import qtium.driver.QTiumLocalDriver;

public class QTiumWindowsChromeLocalDriver extends QTiumLocalDriver {
	// TODO: hard code driver executable
	private String _chromeDriverExecutable;// = "E:\\Workspace\\Eclipse workspace\\QTium
											// Refactoring\\Selenium-TestNG\\Resources\\chromedriver.exe";

	public void setDriverExe(String driverExe) {
		this._chromeDriverExecutable = driverExe;
	}

	@Override
	public String getDeviceName() {
		return QTiumConstants.DEVICE_PC;
	}

	public QTiumWindowsChromeLocalDriver() {

	} // end method

	@Override
	public String getProvider() {
		return "selenium";
	}

	@Override
	public void createWebDriver() {
		File file = null;

		file = new File(_chromeDriverExecutable);
		String sFile = file.getAbsolutePath();
		System.setProperty("webdriver.chrome.driver", sFile);

		if (this._webDriver == null) {
			System.out.println("*** Windows - Chrome driver is NULL");
		} else {
			System.out.println("*** Windows - Chrome driver is NOT NULL");
		}
		try {

			ChromeOptions options = new ChromeOptions();

//			Map<String, String> mobileEmulation = new HashMap<>();
//			mobileEmulation.put("deviceName", "iPhone X");
//			options.setExperimentalOption("mobileEmulation", mobileEmulation);

//			options.addArguments("disable-infobars");
//			options.addArguments("disable-browser-side-navigation");
//			options.addArguments("disable-gpu");
			options.addArguments("--disable-notifications");

//			options.addArguments("start-maximized"); // https://stackoverflow.com/a/26283818/1689770
			options.addArguments("enable-automation"); // https://stackoverflow.com/a/43840128/1689770
//			options.addArguments("--headless"); // only if you are ACTUALLY running headless
			options.addArguments("--no-sandbox"); // https://stackoverflow.com/a/50725918/1689770
			options.addArguments("--disable-infobars"); // https://stackoverflow.com/a/43840128/1689770
			options.addArguments("--disable-dev-shm-usage"); // https://stackoverflow.com/a/50725918/1689770
			options.addArguments("--disable-browser-side-navigation"); // https://stackoverflow.com/a/49123152/1689770
			options.addArguments("--disable-gpu"); // https://stackoverflow.com/questions/51959986/how-to-solve-selenium-chromedriver-timed-out-receiving-message-from-renderer-exc

//			options.addArguments("--headless");
//			options.setHeadless(true);
//			options.addArguments("start-fullscreen");
			this._webDriver = new ChromeDriver(options);
			System.err.println("[info] PC - Chrome driver is created.");

		} catch (Exception ex) {
			System.out.println("PC - Chrome - createWebDriver - exception: " + ex.getMessage());
		}
	}

	@Override
	public String getApplicationType() {
		return QTiumConstants.APP_TYPE_CHROME;
	}
} // end class
