package qtium.driver.selenium.mobile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import qtium.QTiumConstants;
import qtium.driver.QTiumLocalDriver;

public class QTiumWindowsChromeLocalDriver extends QTiumLocalDriver {
	// TODO: hard code driver executable
	private String _chromeDriverExecutable;// = "E:\\Workspace\\Eclipse workspace\\QTium
											// Refactoring\\Selenium-TestNG\\Resources\\chromedriver.exe";

	public void setDriverExe(String driverExe) {
		this._chromeDriverExecutable = driverExe;
	}

	@Override
	public String getDeviceName() {
		return QTiumConstants.DEVICE_PC;
	}

	public QTiumWindowsChromeLocalDriver() {

	} // end method

	@Override
	public String getProvider() {
		return "selenium";
	}

	@Override
	public void createWebDriver() {
		File file = null;

		file = new File(_chromeDriverExecutable);
		String sFile = file.getAbsolutePath();
		System.setProperty("webdriver.chrome.driver", sFile);

		if (this._webDriver == null) {
			System.out.println("*** Mobile - Chrome driver is NULL");
		} else {
			System.out.println("*** Mobile - Chrome driver is NOT NULL");
		}
		try {
			
			ChromeOptions options = new ChromeOptions();
			
			Map<String, String> mobileEmulation = new HashMap<>();
			mobileEmulation.put("deviceName", "iPhone X");
			options.setExperimentalOption("mobileEmulation", mobileEmulation);
			
			options.addArguments("disable-infobars");
			options.addArguments("disable-browser-side-navigation");
			options.addArguments("disable-gpu");
//			options.addArguments("--headless");
//			options.setHeadless(true);
//			options.addArguments("start-fullscreen");
			this._webDriver = new ChromeDriver(options);
			System.err.println("[info] PC - Chrome driver is created.");
			
		} catch (Exception ex) {
			System.out.println("PC - Chrome - createWebDriver - exception: " + ex.getMessage());
		}
	}

	@Override
	public String getApplicationType() {
		return QTiumConstants.APP_TYPE_CHROME;
	}
} // end class
