package qtium.driver.selenium.mac;

import org.openqa.selenium.safari.SafariDriver;

import qtium.QTiumConstants;
import qtium.driver.QTiumLocalDriver;

public class QTiumMacSafariLocalDriver extends QTiumLocalDriver {

	@Override
	public String getDeviceName() {
		return QTiumConstants.DEVICE_MAC;
	}

	public QTiumMacSafariLocalDriver() {

	} // end method

	@Override
	public String getProvider() {
		return "selenium";
	}

	@Override
	public void createWebDriver() {
		if (this._webDriver == null) {
			System.out.println("*** Mac - Safari driver is NULL ***");
		} else {
			System.out.println("*** Mac - Safari driver is NOT NULL ***");
		}
		try {
			this._webDriver = new SafariDriver();
			System.out.println("[info] Safari driver is created.");
		} catch (Exception ex) {
			System.err.println("Safari - createWebDriver - exception: "
					+ ex.getMessage());
		}
	}

	@Override
	public String getApplicationType() {
		return QTiumConstants.APP_TYPE_SAFARI;
	}
} // end class
